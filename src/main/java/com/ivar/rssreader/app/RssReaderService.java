package com.ivar.rssreader.app;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Path("/reader")
public class RssReaderService {

	@Autowired
	RssReaderBean rssBean;

	@SuppressWarnings("rawtypes")
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<RssBean> getData(@QueryParam("rssUrl") String url) {

		List<RssBean> entries = rssBean.getEntries(url);
		System.out.println(entries);
		return entries;
		
	}

}
