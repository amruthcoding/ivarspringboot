package com.ivar.rssreader.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Component;

import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

@Component
public class RssReaderBean {

	private static String USER_AGENT = "Mozilla/5.0";

	
	public List<RssBean> getEntries(String url) {
		CloseableHttpResponse response1 = null;
		CloseableHttpClient httpclient = null;
		SyndFeed feed = null;

		try {
			HttpHost proxy = new HttpHost("proxye1.finra.org", 8080, "http");
			httpclient = HttpClientBuilder.create().setProxy(proxy).build();

			RequestConfig config = RequestConfig.custom().setProxy(proxy)
					.build();
		//	HttpGet httpGet = new HttpGet("http://feeds.finra.org/FINRANews");
			HttpGet httpGet = new HttpGet(url);
			httpGet.setConfig(config);
			// add request header
			httpGet.addHeader("User-Agent", USER_AGENT);
			httpGet.setHeader("Accept",
					"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.setHeader("Accept-Language", "en-US,en;q=0.5");

			response1 = httpclient.execute(httpGet);

			SyndFeedInput input = new SyndFeedInput();
			feed = input
					.build(new XmlReader(response1.getEntity().getContent()));
			
			httpclient.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("ERROR: " + ex.getMessage());

		}
		return convert(feed.getEntries());
	}
	
	private List<RssBean> convert(List<SyndEntryImpl> entries) {
		List<RssBean> list = new ArrayList<RssBean>();
		
		for(SyndEntryImpl entry : entries) {
			RssBean bean = new RssBean();
			bean.setTitle(entry.getTitle());
			bean.setDescription(entry.getDescription().getValue());
			bean.setLink(entry.getLink());
			list.add(bean);
		}
		
		return list;
		
	}

}
