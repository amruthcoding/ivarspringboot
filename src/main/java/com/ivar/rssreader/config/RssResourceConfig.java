package com.ivar.rssreader.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.ivar.rssreader.app.RssReaderService;

@Component
@ApplicationPath("/restful-services")
public class RssResourceConfig extends ResourceConfig {

	public RssResourceConfig() {
		register(RssReaderService.class);
	}
}
